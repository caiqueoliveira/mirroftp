package mirror;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/* ClienteFTP -> Interface(comunica-se com o usuário) > Interface de Comandos do Cliente > Módulo de transferência de dados
 * 
 */
public class FTP {
	/*
	 * FTP Servidor: ftp.xpg.com.bruser jcaiqueoliveirasenha caiqueoliveira
	 */

	private Socket cmdSocket; // Socket de comandos
	private Socket dataSocket; // Socket de dados
	private InputStream cmdIn;
	private OutputStream cmdOut;
	private int codigo = 0;

	/*
	 * O host e a porta passados pelo usuário podem ser armazenados para uma
	 * rotina de* reestabelecimento de conexão em caso de falhas na conexão.
	 */
	public FTP(String host, int port) {
		this.connect(host, port);
	}

	public int getCodigo() {
		return this.codigo;
	}

	public void connect(String host, int port) {

		try {
			this.cmdSocket = new Socket(host, port);

			// Armazena em cmdIn os dados no canal de entrada
			this.cmdIn = this.cmdSocket.getInputStream();

			// Armazena em cmdOut os dados no canal de saída
			this.cmdOut = this.cmdSocket.getOutputStream();
			this.codigo = Integer.parseInt(this.getCmdResp().substring(0, 3));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Captura a resposta do servidor
	private String getCmdResp() throws IOException {
		BufferedReader br = new BufferedReader(
				new InputStreamReader(this.cmdIn));
		String resp = br.readLine();
		// System.out.println(resp);
		return resp;
	}

	// Método que devolve como resposta o diretório corrente
	public String PWD() throws IOException {
		String mensagem = "PWD\r\n";
		this.cmdOut.write(mensagem.getBytes());
		return this.getCmdResp();
	}

	public String MDTM(String pathname) throws IOException {
		String mensagem = "MDTM " + pathname + "\r\n";
		this.cmdOut.write(mensagem.getBytes());
		return this.getCmdResp();
	}

	public int CWD(String newDir) throws IOException {
		String mensagem = "CWD " + newDir + "\r\n";
		this.cmdOut.write(mensagem.getBytes());

		this.codigo = Integer.parseInt(this.getCmdResp().substring(0, 3));
		return this.codigo;
	}

	// Faz um requisição ao servidor de uma porta que ele está escutando para
	// troca de dados
	public void pasv() throws IOException {
		String mensagem = "PASV\r\n";
		this.cmdOut.write(mensagem.getBytes());
		String resp = this.getCmdResp();

		// Do site: http://sdufs.edublogs.org/
		StringTokenizer st = new StringTokenizer(resp);
		st.nextToken("(");
		String ip = st.nextToken(",").substring(1) + "." + st.nextToken(",")
				+ "." + st.nextToken(",") + "." + st.nextToken(",");
		int value1 = Integer.parseInt(st.nextToken(","));
		int value2 = Integer.parseInt(st.nextToken(")").substring(1));
		int port = value1 * 256 + value2;

		// Cria uma conexão de dados com o servidor com o socket na porta
		// obtida
		// pelo PASV
		this.dataSocket = new Socket(ip, port);
	}

	public void DELETE(String nome) throws IOException {
		nome = nome.replace('\\', '/');
		String mensagem = "DELE " + nome + "\r\n";
		this.cmdOut.write(mensagem.getBytes());
		this.getCmdResp();
	}

	// Lista o conteúdo de um determinado diretório
	public String MLSD(String pathDIR) throws IOException {
		this.pasv();
		String mensagem = "MLSD " + pathDIR + "\r\n";
		this.cmdOut.write(mensagem.getBytes());
		this.getCmdResp(); // Resposta do servidor ao comando LIST

		BufferedReader br = new BufferedReader(new InputStreamReader(
				this.dataSocket.getInputStream()));
		String resp = "";
		String line = null;

		while ((line = br.readLine()) != null) {
			resp = resp.concat(line).concat("\n");
		}

		this.dataSocket.close();
		this.getCmdResp(); // Resposta do servidor referente a transferência
							// dos
							// dados (lista de diretórios, nesse caso)
		// System.out.println(resp);

		return resp;
	}

	// Lista o conteúdo de um determinado diretório
	public ArrayList<String> LIST(String pathDIR) throws IOException {
		this.pasv();
		String mensagem = "LIST " + pathDIR + "\r\n";
		this.cmdOut.write(mensagem.getBytes());
		this.getCmdResp(); // Resposta do servidor ao comando LIST

		BufferedReader br = new BufferedReader(new InputStreamReader(
				this.dataSocket.getInputStream()));
		ArrayList<String> resp = new ArrayList<String>();
		String line = null;

		while ((line = br.readLine()) != null) {
			resp.add(line);

		}

		this.dataSocket.close();
		this.getCmdResp(); // Resposta do servidor referente a transferência
							// dos
							// dados (lista de diretórios, nesse caso)
		// System.out.println("chala, acai do edton"+resp);

		return resp;
	}

	// Comando FTP para criar pasta
	public void MKD(String nome) throws IOException {
		nome = nome.replace('\\', '/');
		String mensagem = "MKD " + nome + "\r\n";
		this.cmdOut.write(mensagem.getBytes());
		this.getCmdResp();
	}

	public void STOR(String diretorioRemoto, String diretorioLocal,
			String fileName) throws IOException {
		this.pasv();
		diretorioRemoto = diretorioRemoto.replace('\\', '/');
		fileName = fileName.replace('\\', '/');

		File file = new File(diretorioLocal + fileName);

		String comando = "STOR " + diretorioRemoto + fileName + "\r\n";
		this.cmdOut.write(comando.getBytes());
		this.getCmdResp(); // Resposta do servidor ao comando STOR

		FileInputStream fis = new FileInputStream(file);

		byte[] buffer = new byte[1000];
		int tam = 0;

		OutputStream dataOut = this.dataSocket.getOutputStream();

		// tam recebe a quantidade de bytes lidos por vez
		while ((tam = fis.read(buffer)) > -1) {
			dataOut.write(buffer, 0, tam);
		}

		fis.close();
		this.dataSocket.close();

		this.getCmdResp(); // Resposta do servidor referente a transferência
							// dos
							// dados
	}

	// Baixar arquivo de um servidor FTP
	public boolean RETR(String diretorioRemoto, String diretorioLocal,
			String fileName) throws IOException {
		this.pasv();
		diretorioRemoto = diretorioRemoto.replace('\\', '/');
		fileName = fileName.replace('\\', '/');
		String comando = "RETR " + diretorioRemoto + fileName + "\r\n";
		this.cmdOut.write(comando.getBytes());
		String resp = this.getCmdResp(); // Resposta do servidor ao comando RETR

		if (Integer.parseInt(resp.substring(0, 3)) != 150) {
			return false;
		}

		// System.out.println("MKDIR: " + diretorioLocal + fileName);
		File file = new File(diretorioLocal + fileName);
		file.createNewFile();
		FileOutputStream fos = new FileOutputStream(file);

		byte[] buffer = new byte[1000];
		int tam = 0;

		InputStream dataIn = this.dataSocket.getInputStream();

		// tam recebe a quantidade de bytes lidos por vez
		while ((tam = dataIn.read(buffer)) > -1) {
			fos.write(buffer, 0, tam);
		}

		fos.close();
		this.dataSocket.close();

		this.getCmdResp(); // Resposta do servidor referente a transferência
							// dos
		// dados

		return true;

	}

	public int login(String user, String password) throws IOException {
		String usuario = "USER " + user + "\r\n";
		this.cmdOut.write(usuario.getBytes());
		this.getCmdResp();

		String senha = "PASS " + password + "\r\n";
		this.cmdOut.write(senha.getBytes());
		this.codigo = Integer.parseInt(this.getCmdResp().substring(0, 3));
		return this.codigo;
	}

	public void type(String type) throws IOException {
		String comando = "TYPE " + type + "\r\n";

		this.cmdOut.write(comando.getBytes());
		this.getCmdResp();
	}

	public String RMD(String nome) throws IOException {
		nome = nome.replace('\\', '/');
		String mensagem = "RMD " + nome + "\r\n";
		this.cmdOut.write(mensagem.getBytes());

		String res = this.getCmdResp();

		// System.out.println(res);

		return res.substring(0, 3);
	}

	public void disconnect(){
		try {
			this.cmdSocket.close();
			this.dataSocket.close();
		} catch (IOException e) {
		}
	}
	// cliente.LIST("/");
	// cliente.PWD();
	// cliente.PWD(); // Informa que o diretório atual é "/"
	// cliente.MKD("docs"); // cria pasta chamada "docs" no diretório raíz
	// "/"
	// cliente.CWD("/docs"); // Muda para o novo diretório criado "docs"
	// cliente.PWD(); // Verifica se realmente está em docs
	// cliente.MKD("PDFs"); // Cria o diretório "PDFs" dentro do diretório
	// "docs"
	// cliente.CWD("PDFs"); // Muda para o novo diretório criado "PDFs"
	// cliente.PWD(); // Verifica se realmente está em PDFs
	// cliente.type("I"); // Modo Binário para transferência de arquivo
	// cliente.RETR("/aplicativos.txt");
}
