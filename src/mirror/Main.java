// MIRROR FTP
// Autores: Claudio Mota Oliveira, Jos� Caique Oliveira da Silva
// Ultima atualiza��o: 05/02/2015

package mirror;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class Main {
	private static final int SINC = 1;
	private static final int NSINC = -1;
	private static final int DIRETORIO = -1;

	public static ArrayList<PropriedadeArquivo> atualizarMirror(
			PropriedadeArquivo metaServidor, PropriedadeArquivo metaLocal,
			ArrayList<PropriedadeArquivo> arquivosLocais,
			ArrayList<PropriedadeArquivo> arquivosServidor, FTP cliente)
			throws Exception {
		int i, j;
		ArrayList<PropriedadeArquivo> aux = new ArrayList<PropriedadeArquivo>();
		if (metaServidor.getData().before(metaLocal.getData())) {
			// Cliente � mais recente
			for (i = 0; i < arquivosLocais.size(); i++) {
				boolean ok = false;
				for (j = 0; j < arquivosServidor.size(); j++) {
					if (arquivosLocais.get(i).getDiretorio()
							.equals(arquivosServidor.get(j).getDiretorio())) {
						ok = true;
						arquivosServidor.get(j).setFlag(SINC);
						if (arquivosLocais.get(i).getData()
								.after(arquivosServidor.get(j).getData())
								&& !arquivosLocais.get(i).isDiretorio()) {
							cliente.STOR(Arquivo.getInstance()
									.getDiretorioServidor() + "/", Arquivo
									.getInstance().getDiretorio() + "/",
									arquivosLocais.get(j).getDiretorio());
							// System.out.println("Atualizou S " +
							// arquivosServidor.get(j).getDiretorio());
							break;
						}
					}
				}
				if (!ok) { // Em caso do arquivo ter sido criado no servdor mas
							// nao existe na maquina local
					if (arquivosLocais.get(i).getLongData() == DIRETORIO) {
						cliente.MKD(Arquivo.getInstance()
								.getDiretorioServidor()
								+ "/"
								+ arquivosLocais.get(i).getDiretorio());
						// System.out.println("Novo Diretorio na nuvi " +
						// arquivosServidor.get(j).getDiretorio());
					} else {
						// System.out.println("Guardou S " +
						// arquivosServidor.get(j).getDiretorio());
						cliente.STOR(Arquivo.getInstance()
								.getDiretorioServidor() + "/", Arquivo
								.getInstance().getDiretorio() + "/",
								arquivosLocais.get(i).getDiretorio());
					}
				}
			}

			for (j = 0; j < arquivosServidor.size(); j++) {
				if (arquivosServidor.get(j).getFlag() == NSINC) {
					if (!arquivosServidor.get(j).isDiretorio()) {
						cliente.DELETE(Arquivo.getInstance()
								.getDiretorioServidor()
								+ "/"
								+ arquivosServidor.get(j).getDiretorio());
					} else {
						aux.add(arquivosServidor.get(j));
					}
				}
			}

			for (j = aux.size() - 1; j >= 0; j--) {
				cliente.RMD(Arquivo.getInstance().getDiretorioServidor() + "/"
						+ aux.get(j).getDiretorio());
			}
			return arquivosLocais;
		} else if (metaServidor.getData().after(metaLocal.getData())) {
			for (i = 0; i < arquivosServidor.size(); i++) {
				// Servidor � mais recente
				boolean ok = false;
				for (j = 0; j < arquivosLocais.size(); j++) {
					if (arquivosServidor.get(i).getDiretorio()
							.equals(arquivosLocais.get(j).getDiretorio())) {
						ok = true;
						arquivosLocais.get(j).setFlag(SINC);
						if (arquivosServidor.get(i).getData()
								.after(arquivosLocais.get(j).getData())
								&& !arquivosLocais.get(i).isDiretorio()) {
							// System.out.println("Atualizou L");
							System.out.println("Atulizando..");
							cliente.RETR(Arquivo.getInstance()
									.getDiretorioServidor() + "/", Arquivo
									.getInstance().getDiretorio() + "/",
									arquivosLocais.get(j).getDiretorio());
							break;
						}
					}
				}
				if (!ok) { // Em caso do arquivo ter sido criado no servdor mas
							// nao existe na maquina local
					if (arquivosServidor.get(i).getLongData() == DIRETORIO) {
						File file = new File(Arquivo.getInstance()
								.getDiretorio()
								+ "/"
								+ arquivosServidor.get(i).getDiretorio());
						file.mkdir();
						// System.out.println("Neu Diretorio L " +
						// arquivosServidor.get(j).getDiretorio());
					} else {
						cliente.RETR(Arquivo.getInstance()
								.getDiretorioServidor() + "/", Arquivo
								.getInstance().getDiretorio() + "/",
								arquivosServidor.get(i).getDiretorio());
						// System.out.println("Guardou L " +
						// arquivosServidor.get(j).getDiretorio());
					}
				}
			}

			for (j = 0; j < arquivosLocais.size(); j++) {
				if (arquivosLocais.get(j).getFlag() == NSINC) {
					if (arquivosLocais.get(j).isDiretorio()) {
						aux.add(arquivosLocais.get(j));
					} else {
						File file = new File(Arquivo.getInstance()
								.getDiretorio()
								+ "/"
								+ arquivosLocais.get(j).getDiretorio());
						file.delete();
					}
					// System.out.println("Delete L " +
					// arquivosServidor.get(j).getDiretorio());
				}
			}

			for (j = aux.size() - 1; j >= 0; j--) {
				File file = new File(Arquivo.getInstance().getDiretorio() + "/"
						+ arquivosLocais.get(j).getDiretorio());
				file.delete();
			}
			return arquivosServidor;
		}
		return null;
	}

	public static void main(String[] args) {
		// ArrayList diretorios =
		// Arquivo.getInstance().getDiretorio();
		/*
		 * Thread t = new Thread(new Runnable() {
		 * 
		 * public void run() { try { //verificarDiretorios
		 * Thread.sleep(Arquivo.getInstance().getIntervalo()); } catch
		 * (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } } });
		 */
		/*
		 * Diretorio dir = new Diretorio(); dir.listarDiretorioLocal(".");
		 */

		while (3 != 5) {
			try {
				FTP cliente = new FTP(Arquivo.getInstance().getIP(), Arquivo
						.getInstance().getPorta());
				if (cliente.getCodigo() == 220) {

					if (cliente.login(Arquivo.getInstance().getUsuario(),
							Arquivo.getInstance().getSenha()) == 230) {
					} else {
						// System.err.println("Dados de Login incorreto");
					}
					if (cliente.CWD(Arquivo.getInstance()
							.getDiretorioServidor()) == 250) {
						cliente.LIST(Arquivo.getInstance()
								.getDiretorioServidor());
					}

					// =========================comeca

					Diretorio gerenciadorDiretorio = new Diretorio();
					ArrayList<PropriedadeArquivo> arquivosLocaisMeta = MetaData
							.MetadataToArray();
					ArrayList<PropriedadeArquivo> arquivosLocais = gerenciadorDiretorio
							.listarDiretorioLocal(Arquivo.getInstance()
									.getDiretorio(), 0);
					PropriedadeArquivo metaDataLocal = new PropriedadeArquivo(
							"METADATA",
							gerenciadorDiretorio.getDataMetada(arquivosLocais, MetaData.getDataMetadata()));
					if (arquivosLocaisMeta.size() != arquivosLocais.size()) {
						metaDataLocal.setData((new Date()).getTime());
					}

					ArrayList<PropriedadeArquivo> arquivosRemotos;
					PropriedadeArquivo metaDataRemota;
					if (!cliente.RETR("/", Arquivo.getInstance().getDiretorio()
							+ "/", "METADATA")) {
						File file = new File(Arquivo.getInstance()
								.getDiretorio() + "/" + "METADATA");
						file.createNewFile();
						arquivosRemotos = new ArrayList<PropriedadeArquivo>();
						metaDataRemota = new PropriedadeArquivo("METADATA", 0);
					} else {
						arquivosRemotos = MetaData.MetadataToArray();
						metaDataRemota = new PropriedadeArquivo("METADATA",
								MetaData.getDataMetadata());
					}

					ArrayList<PropriedadeArquivo> atualizacao = atualizarMirror(
							metaDataRemota, metaDataLocal, arquivosLocais,
							arquivosRemotos, cliente);
					if (atualizacao != null) {
						MetaData.generateMetaData(Arquivo.getInstance()
								.getDiretorio() + "/", atualizacao);
						cliente.STOR("/", Arquivo.getInstance().getDiretorio()
								+ "/", "METADATA");
					}
					cliente.disconnect();
					Thread.sleep(Arquivo.getInstance().getIntervalo());
				}
			} catch (Exception e) {
				// e.printStackTrace();
			}
		}
	}
}
