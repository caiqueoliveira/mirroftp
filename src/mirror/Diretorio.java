package mirror;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;

public class Diretorio {
	private ArrayList<PropriedadeArquivo> aux = new ArrayList<PropriedadeArquivo>();

	public long getDataMetada(ArrayList<PropriedadeArquivo> aux, long data) {
		long max = data;
		int i;

		for (i = 0; i < aux.size(); i++) {
			if (aux.get(i).getLongData() > max) {
				max = aux.get(i).getLongData();
			}
		}

		return max;
	}

	public ArrayList<PropriedadeArquivo> listarDiretorioLocal(String atual,
			int offset) {
		try {
			if (offset == 0) {
				offset = atual.length() + 1;
			}
			File f = new File(atual); // current directory
			File[] files = f.listFiles();
			for (File file : files) {
				if (file.isFile()
						&& !file.getCanonicalPath().substring(offset)
								.equals("METADATA")) {

					BasicFileAttributes attrs = Files.readAttributes(
							file.toPath(), BasicFileAttributes.class);
					FileTime time = attrs.lastModifiedTime();
					aux.add(new PropriedadeArquivo(file.getCanonicalPath()
							.substring(offset), time.toMillis()));
				}
			}
			for (File file : files) {
				if (file.isDirectory()) {
					try {
						aux.add(new PropriedadeArquivo(file.getCanonicalPath()
								.substring(offset), -1));
						listarDiretorioLocal(file.getCanonicalPath(), offset);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// comando para pegar diretorios e adicionar a dir
		return aux;
	}
}
