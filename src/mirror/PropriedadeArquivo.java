package mirror;

import java.util.Date;

public class PropriedadeArquivo {
	private String diretorio;
	private long data;
	int flag; // 0 : n�o iterado, 1: iterado

	public PropriedadeArquivo() {
		this.data = -1;
		this.flag = -1;
		this.diretorio = "";
	}

	public PropriedadeArquivo(String nome, long data) {
		this.flag = -1;
		this.diretorio = nome;
		this.data = data;
	}

	public String getDiretorio() {
		return diretorio;
	}

	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}

	public Date getData() {
		return new Date(data);
	}

	public long getLongData() {
		return data;
	}

	public void setData(long data) {
		this.data = data;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public boolean isDiretorio() {
		return data == -1;
	}

}
