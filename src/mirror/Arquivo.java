package mirror;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

//singleton
public class Arquivo {
	private static Arquivo instance = null;
	private final String nome = "entradas.txt";
	private String[] dados = new String[7];

	public Arquivo() throws IOException {

		try {
			FileReader arq = new FileReader(nome);
			BufferedReader lerArq = new BufferedReader(arq);
			String linha = lerArq.readLine(); // lê a primeira linha
			int i = 0;
			while (linha != null) {
				dados[i] = linha;
				i++;
				linha = lerArq.readLine(); // lê da segunda até a última
											// linha
			}
			arq.close();
		} catch (IOException e) {
			System.out.println("Erro na abertura do arquivo");
		}
	}

	public static Arquivo getInstance() {
		if (instance == null) {
			try {
				instance = new Arquivo();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instance;
	}

	public String getIP() {
		return dados[0];
	}

	public int getPorta() {
		return Integer.parseInt(dados[1]);
	}

	public int getIntervalo() {
		return Integer.parseInt(dados[2]) * 1000;
	}

	public String getUsuario() {
		return dados[3];
	}

	public String getSenha() {
		return dados[4];
	}

	public String getDiretorio() {
		return dados[5];
	}

	public String getDiretorioServidor() {
		return dados[6];
	}
}
