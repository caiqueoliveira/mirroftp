package mirror;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class MetaData {
	private static long dataMetaData;

	private static void setDataMetaData(long d) {
		dataMetaData = d;
	}

	public static long getDataMetadata() {
		return dataMetaData;
	}

	public static void generateMetaData(String diretorio,
			ArrayList<PropriedadeArquivo> List) throws IOException {

		File file = new File(diretorio + "METADATA");
		FileOutputStream fos = new FileOutputStream(file);
		String linha = (new Long((new Date()).getTime())).toString() + "\n";
		fos.write(linha.getBytes());
		// tam recebe a quantidade de bytes lidos por vez
		Iterator<PropriedadeArquivo> iter = List.iterator();
		while (iter.hasNext()) {
			PropriedadeArquivo aux = iter.next();
			linha = aux.getDiretorio() + "\n" + aux.getLongData() + "\n";
			fos.write(linha.getBytes());
		}

		fos.close();

		// fazer algo
	}

	public static ArrayList<PropriedadeArquivo> MetadataToArray() {
		ArrayList<PropriedadeArquivo> aux = new ArrayList<PropriedadeArquivo>();
		try {
			File file = new File(Arquivo.getInstance().getDiretorio()
					+ "/METADATA");
			if (!file.exists()) {
				setDataMetaData(Long.valueOf((long) 0));
				FileOutputStream fos = new FileOutputStream(file);
				String linha = (new Long(0)).toString() + "\n";
				fos.write(linha.getBytes());
				fos.close();
				return aux;
			}
			FileReader arq = new FileReader(Arquivo.getInstance()
					.getDiretorio() + "/METADATA");
			BufferedReader lerArq = new BufferedReader(arq);
			String data1 = lerArq.readLine();
			setDataMetaData(Long.parseLong(data1));
			String diretorio;
			while ((diretorio = lerArq.readLine()) != null) {
				String data = lerArq.readLine();
				aux.add(new PropriedadeArquivo(diretorio, Long.parseLong(data)));
				;
			}
			arq.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aux;
	}
}
